# RetroShield 6809 for Arduino Mega

I'm excited to announce a very cool project for retromaniacs:

* RetroShield 6809 is a hardware shield for Arduino Mega. 
* It enables Arduino to emulate peripherals and run 6809  programs using a real 6809! 
* You can try 6809  assembly and run some of the old 6809  programs 
without building complicated circuits. 
* You can use existing Arduino shields to extend the capabilities. 
* Arduino Mega gives you 4~6KB of RAM and >200KB ROM (in flash area).
* All hardware and software files are provided for you to study and play.
* Hardware details and PCB/kits for sale: http://www.8bitforce.com/

### Installation

First:
* DO NOT PLUG RETROSHIELD before programming Arduino first!

Otherwise any prexisting code may drive GPIO's randomly and short-circuit
your precious microprocessor.

### 6809 Links

* SB-Assembler: https://www.sbprojects.net/sbasm/

### License Information

* Copyright 2019 Erturk Kocalar, 8Bitforce.com
* Hardware is released under [Creative Commons ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/).
* Software is released under the [MIT License](http://opensource.org/licenses/MIT).
* Distributed as-is; no warranty is given.
* See LICENSE.md for details.
* All text above must be included in any redistribution.
